# pylint: disable=invalid-name
import os
import pickle
import warnings
from collections import Counter

import click
import mlflow
import pandas as pd
from dotenv import load_dotenv
from feature_engine.encoding import OneHotEncoder
from feature_engine.imputation import (
    AddMissingIndicator,
    CategoricalImputer,
    MeanMedianImputer,
)
from mlflow.models.signature import infer_signature
from sklearn.metrics import (
    accuracy_score,
    f1_score,
    precision_score,
    recall_score,
    roc_auc_score,
)
from sklearn.pipeline import Pipeline
from xgboost import XGBClassifier

from src.constants import (
    CATEGORICAL_VARIABLES,
    CATEGORICAL_VARIABLES_WITH_NA,
    NUMERICAL_VARIABLES,
    NUMERIC_VARIABLES_WITH_NA,
    RANDOM_STATE,
    TARGET,
)
from src.models.metrics import metrics_calculation

warnings.simplefilter(action="ignore", category=pd.errors.PerformanceWarning)

load_dotenv()
remote_server_uri = os.getenv("MLFLOW_TRACKING_URI")
os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://46.229.214.95:9000"


@click.command()
@click.argument("train_data", type=click.Path(exists=True))
@click.argument("validation_data", type=click.Path(exists=True))
@click.argument("model_output", type=click.Path())
def train_model(
    train_data: pd.DataFrame, validation_data: pd.DataFrame, model_output: str
):
    """
    Создание пайплайна, тренировка модели, расчет метрик качества на
    валидационной выборке
    @param train_data:
    @param validation_data:
    @param model_output:
    @return:
    """
    with mlflow.start_run():
        # split data for train and validation
        train_data = pd.read_csv(train_data)
        validation_data = pd.read_csv(validation_data)

        X_train, X_val = (
            train_data.drop(TARGET, axis=1),
            validation_data.drop(TARGET, axis=1),
        )
        y_train, y_val = train_data[TARGET], validation_data[TARGET]
        # count for balance class
        counts = Counter(y_train)
        # set up the pipeline
        churn_pipe = Pipeline(
            [
                # ===== IMPUTATION =====
                # add missing indicator to numerical variables
                (
                    "missing_indicator_cat",
                    AddMissingIndicator(variables=CATEGORICAL_VARIABLES),
                ),
                # impute categorical variables with string 'missing'
                (
                    "categorical_imputation",
                    CategoricalImputer(
                        imputation_method="missing",
                        variables=CATEGORICAL_VARIABLES_WITH_NA,
                    ),
                ),
                # add missing indicator to numerical variables
                (
                    "missing_indicator_num",
                    AddMissingIndicator(variables=NUMERICAL_VARIABLES),
                ),
                # impute numerical variables with the median
                (
                    "median_imputation",
                    MeanMedianImputer(
                        imputation_method="median", variables=NUMERIC_VARIABLES_WITH_NA
                    ),
                ),
                # == CATEGORICAL ENCODING ======
                # encode categorical variables using one hot encoding into k-1 variables
                ("categorical_encoder", OneHotEncoder(variables=CATEGORICAL_VARIABLES)),
                # estimator
                (
                    "xgboost",
                    XGBClassifier(
                        nthread=8,
                        scale_pos_weight=counts[0] / counts[1],
                        random_state=RANDOM_STATE,
                    ),
                ),
            ]
        )
        # train model and metrics check on valid sample
        churn_pipe.fit(X_train, y_train)
        metrics_calculation(churn_pipe, X_train, X_val, y_train, y_val)
        # make predictions for val set
        class_prediction = churn_pipe.predict(X_val)
        proba_prediction = churn_pipe.predict_proba(X_val)[:, 1]

        scores = {
            "roc_auc": roc_auc_score(y_val, proba_prediction),
            "accuracy": accuracy_score(y_val, class_prediction),
            "precision_score": precision_score(y_val, class_prediction),
            "recall_score": recall_score(y_val, class_prediction),
            "f1_score": f1_score(y_val, class_prediction),
        }
        signature = infer_signature(X_val, proba_prediction)

        # train model on all dataset
        # all_dataset = pd.concat([train_data, validation_data])
        X = pd.concat([X_train, X_val])
        y = pd.concat([y_train, y_val])
        churn_pipe.fit(X, y)

        # log metrics and model
        mlflow.log_metrics(scores)
        mlflow.sklearn.log_model(churn_pipe, "model", signature=signature)
        with open(model_output, "wb") as file:
            pickle.dump(churn_pipe, file)


if __name__ == "__main__":
    train_model()
