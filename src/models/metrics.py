# pylint: disable=invalid-name
import numpy as np
from sklearn.metrics import (
    accuracy_score,
    classification_report,
    roc_auc_score,
)


def metrics_calculation(churn_pipe, X_train, X_val, y_train, y_val):
    """
    Расчет метрик roc_auc, accuracy, classification report
    @param churn_pipe: пайплайн
    @param X_train: тренировочный набор данных
    @param X_val: валидационный набор данных
    @param y_train: тренировочные метки оттока
    @param y_val: валидационные метки оттока
    @return: None
    """

    # make predictions for train set
    class_prediction_train = churn_pipe.predict(X_train)
    proba_prediction_train = churn_pipe.predict_proba(X_train)[:, 1]
    # determine roc-auc and accuracy

    print(f"train roc-auc: {roc_auc_score(y_train, proba_prediction_train)}")
    print(f"train accuracy: {accuracy_score(y_train, class_prediction_train)}")
    print()

    # make predictions for test set
    class_prediction_val = churn_pipe.predict(X_val)
    proba_prediction_val = churn_pipe.predict_proba(X_val)[:, 1]

    # determine roc-auc and accuracy
    print(f"test roc-auc: {roc_auc_score(y_val, proba_prediction_val)}")
    print(f"test accuracy: {accuracy_score(y_val,  class_prediction_val)}")
    print()
    print(classification_report(y_val, np.where(proba_prediction_val > 0.5, 1, 0)))
